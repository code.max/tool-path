# tool-path

PHP - Herramientas para manipular rutas de archivos y directorios.

 - Path::safe( string $path ): string
 - Path::forceDirBase( string $dirPath ): string
 - Path::fileExtension( string $path ): string
 - Path::forceCreateFilepath( string $filepath ): void
 - Path::deleteEntireDirectory( string $dirPath ): void
